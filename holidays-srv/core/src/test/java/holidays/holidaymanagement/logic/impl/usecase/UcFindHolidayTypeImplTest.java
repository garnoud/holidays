package holidays.holidaymanagement.logic.impl.usecase;

import javax.inject.Inject;

import org.junit.Test;

import holidays.general.common.base.AbstractRestServiceTest;
import holidays.general.logic.api.UseCase;
import holidays.holidaymanagement.logic.api.to.HolidayTypeSearchCriteriaTo;
import holidays.holidaymanagement.logic.api.usecase.UcFindHolidayType;
import io.oasp.module.jpa.common.api.to.PaginationTo;

/**
 * @author GARNOUD
 *
 */
public class UcFindHolidayTypeImplTest extends AbstractRestServiceTest {

  @Inject
  @UseCase
  UcFindHolidayType ucf;

  @Test
  public void test() {

    HolidayTypeSearchCriteriaTo criteria = new HolidayTypeSearchCriteriaTo();
    criteria.setPagination(new PaginationTo());
    criteria.getPagination().setTotal(true);

    assertThat(this.ucf.findHolidayTypeEtos(criteria).getPagination().getTotal()).isEqualTo(6);

  }

}
