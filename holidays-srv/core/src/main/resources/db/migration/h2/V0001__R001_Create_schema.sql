create table role (
  id BIGINT NOT NULL AUTO_INCREMENT,
   modificationCounter INTEGER default 0 NOT NULL,
   code varchar(2) not null,
   name varchar(100) not null,
   constraint PK_role primary key(id),
   CONSTRAINT UC_role_code UNIQUE(code),
   CONSTRAINT UC_role_name UNIQUE(name)
);

create table userAccount (
   id BIGINT NOT NULL AUTO_INCREMENT,
   modificationCounter INTEGER default 0 NOT NULL,
   login varchar(8) not null,
   password varchar(500) not null,
   roleId bigint not null,
   constraint PK_userAccount primary key(id),
   CONSTRAINT UC_userAccount_login UNIQUE(login),
   CONSTRAINT FK_role_roleId FOREIGN KEY(roleId) REFERENCES role(id)
);

create table people (
   id BIGINT NOT NULL AUTO_INCREMENT,
   modificationCounter INTEGER default 0 NOT NULL,
   matricule varchar(10) not null,
   lastName varchar(100) not null,
   firstName varchar(100) not null,
   CONSTRAINT PK_people PRIMARY KEY(id),
   CONSTRAINT UC_people_matricule UNIQUE(matricule)
);

create table peopleUserAccount(
   id BIGINT NOT NULL AUTO_INCREMENT,
   modificationCounter INTEGER default 0 NOT NULL,
   peopleId bigint not null,
   userAccountId bigint not null,
   CONSTRAINT PK_peopleUserAccount PRIMARY KEY(id),
   CONSTRAINT FK_peopleUserAccount_peopleId FOREIGN KEY(peopleId) REFERENCES people(id),
   CONSTRAINT FK_peopleUserAccount_userAccountId FOREIGN KEY(userAccountId) REFERENCES userAccount(id)
);

create table holidayType (
  id BIGINT NOT NULL AUTO_INCREMENT,
  modificationCounter INTEGER default 0 NOT NULL,
  name varchar(100) not null,
  writeable boolean default 1 not null,
  CONSTRAINT PK_holiday_type primary key(id),
  CONSTRAINT Uc_holiday_type unique(name)
);

create table bankHoliday (
  id BIGINT NOT NULL AUTO_INCREMENT,
  modificationCounter INTEGER default 0 NOT NULL,
  beginDate timestamp not null,
  endDate timestamp not null,
  holidayTypeId bigint not null,
  CONSTRAINT PK_bankHoliday PRIMARY KEY(id),
  CONSTRAINT FK_bankHoliday_holidayTypeId FOREIGN KEY(holidayTypeId) REFERENCES holidayType(id)
);

create table availableHoliday (
  id BIGINT NOT NULL AUTO_INCREMENT,
  modificationCounter INTEGER default 0 NOT NULL,
  peopleId bigint not null,
  holidayTypeId bigint not null,
  counter float not null,
  expiryDate timestamp not null,
  CONSTRAINT PK_availableHoliday PRIMARY KEY(id),
  CONSTRAINT FK_availableHoliday_peopleId FOREIGN KEY(peopleId) REFERENCES people(id),
  CONSTRAINT FK_availableHoliday_holidayTypeId FOREIGN KEY(holidayTypeId) REFERENCES holidayType(id)
);

create table plannedHoliday (
  id BIGINT NOT NULL AUTO_INCREMENT,
  modificationCounter INTEGER default 0 NOT NULL,
  beginDate timestamp not null,
  endDate timestamp not null,
  peopleId bigint not null,
  holidayTypeId bigint not null,
  availableHolidayId bigint not null,
  CONSTRAINT PK_plannedHoliday PRIMARY KEY(id),
  CONSTRAINT FK_plannedHoliday_peopleId FOREIGN KEY(peopleId) REFERENCES people(id),
  CONSTRAINT FK_plannedHoliday_holidayTypeId FOREIGN KEY(holidayTypeId) REFERENCES holidayType(id),
  CONSTRAINT FK_plannedHoliday_availableHolidayId FOREIGN KEY(availableHolidayId) REFERENCES availableHoliday(id)
);

create table engagement (
  id BIGINT NOT NULL AUTO_INCREMENT,
  modificationCounter INTEGER default 0 NOT NULL,
  name varchar(100) not null,
  CONSTRAINT PK_engagement PRIMARY KEY(id)
);

create table engagementPeople (
  id BIGINT NOT NULL AUTO_INCREMENT,
  modificationCounter INTEGER default 0 NOT NULL,
  peopleId bigint not null,
  engagementId bigint not null,
  CONSTRAINT PK_engagementPeople PRIMARY KEY(id),
  CONSTRAINT FK_engagementPeople_peopleId FOREIGN KEY(peopleId) REFERENCES people(id),
  CONSTRAINT FK_engagementPeople_engagementId FOREIGN KEY(engagementId) REFERENCES engagement(id)
);


-- This is the SQL script for setting up the DDL for the h2 database
-- In a typical project you would only distinguish between main and test for flyway SQLs
-- However, in this sample application we provde support for multiple databases in parallel
-- You can simply choose the DB of your choice by setting spring.profiles.active=XXX in config/application.properties
-- Assuming that the preconfigured user exists with according credentials using the included SQLs



CREATE SEQUENCE HIBERNATE_SEQUENCE START WITH 1000000;

-- *** Staffmemeber ***
CREATE TABLE StaffMember(
  id BIGINT NOT NULL AUTO_INCREMENT,
  modificationCounter INTEGER NOT NULL,
  firstname VARCHAR(255),
  lastname VARCHAR(255),
  login VARCHAR(255),
  role INTEGER,
  CONSTRAINT PK_StaffMember PRIMARY KEY(id),
  CONSTRAINT UC_StaffMember_login UNIQUE(login)
);

-- *** Product ***
CREATE TABLE Product(
  dType VARCHAR(31) NOT NULL,
  id BIGINT NOT NULL AUTO_INCREMENT,
  modificationCounter INTEGER NOT NULL,
  description VARCHAR(255),
  name VARCHAR(255),
  alcoholic BOOLEAN,
  pictureId BIGINT,
  CONSTRAINT PK_Product PRIMARY KEY(id)
);

CREATE TABLE Product_AUD(
  revType TINYINT,
  description VARCHAR(255),
  name VARCHAR(255),
  pictureId BIGINT,
  alcoholic BOOLEAN,
  dType VARCHAR(31) NOT NULL,
  id BIGINT NOT NULL,
  rev BIGINT NOT NULL
);

-- *** Offer ***
CREATE TABLE Offer(
  id BIGINT NOT NULL AUTO_INCREMENT,
  modificationCounter INTEGER NOT NULL,
  description VARCHAR(255),
  name VARCHAR(255),
  price DECIMAL(19, 2),
  number BIGINT,
  state INTEGER,
  drinkId BIGINT,
  mealId BIGINT,
  sideDishId BIGINT,
  CONSTRAINT PK_Offer PRIMARY KEY(id),
  CONSTRAINT UC_Offer_name UNIQUE(name),
  CONSTRAINT UC_Offer_number UNIQUE(number),
  CONSTRAINT FK_Offer_sideDishId FOREIGN KEY(sideDishId) REFERENCES Product(id) NOCHECK,
  CONSTRAINT FK_Offer_mealId FOREIGN KEY(mealId) REFERENCES Product(id) NOCHECK,
  CONSTRAINT FK_Offer_drinkId FOREIGN KEY(drinkId) REFERENCES Product(id) NOCHECK

);

-- *** RestaurantTable (Table is a reserved keyword in Oracle) ***
CREATE TABLE RestaurantTable(
  id BIGINT NOT NULL AUTO_INCREMENT,
  modificationCounter INTEGER NOT NULL,
  number BIGINT NOT NULL CHECK (number >= 0),
  state INTEGER,
  waiterId BIGINT,
  CONSTRAINT PK_RestaurantTable PRIMARY KEY(id),
  CONSTRAINT UC_Table_number UNIQUE(number)

);

-- *** RestaurantOrder (Order is a reserved keyword in Oracle) ***
CREATE TABLE RestaurantOrder(
  id BIGINT NOT NULL AUTO_INCREMENT,
  modificationCounter INTEGER NOT NULL,
  state INTEGER,
  tableId BIGINT NOT NULL,
  CONSTRAINT PK_RestaurantOrder PRIMARY KEY(id)
);

-- *** OrderPosition ***
CREATE TABLE OrderPosition(
  id BIGINT NOT NULL AUTO_INCREMENT,
  modificationCounter INTEGER NOT NULL,
  comment VARCHAR(255),
  cookId BIGINT,
  offerId BIGINT,
  offerName VARCHAR(255),
  price DECIMAL(19, 2),
  state INTEGER,
  drinkState INTEGER,
  orderId BIGINT,
  CONSTRAINT PK_OrderPosition PRIMARY KEY(id),
  CONSTRAINT FK_OrderPosition_orderId FOREIGN KEY(orderId) REFERENCES RestaurantOrder(id) NOCHECK,
  CONSTRAINT FK_OrderPosition_cookId FOREIGN KEY(cookId) REFERENCES StaffMember(id) NOCHECK

);

-- *** Bill ***
CREATE TABLE Bill(
  id BIGINT NOT NULL AUTO_INCREMENT,
  modificationCounter INTEGER NOT NULL,
  payed BOOLEAN NOT NULL,
  tip DECIMAL(19, 2),
  total DECIMAL(19, 2),
  CONSTRAINT PK_Bill PRIMARY KEY(id)
);

CREATE TABLE BillOrderPosition(
  billId BIGINT NOT NULL AUTO_INCREMENT,
  orderPositionsId BIGINT NOT NULL,
  CONSTRAINT FK_BillOrderPosition_billId FOREIGN KEY(billId) REFERENCES Bill(id) NOCHECK,
  CONSTRAINT FK_BillOrderPosition_orderPositionsId FOREIGN KEY(orderPositionsId) REFERENCES OrderPosition(id) NOCHECK
);

-- *** BinaryObject (BLOBs) ***
CREATE TABLE BinaryObject (
  id BIGINT NOT NULL AUTO_INCREMENT,
  modificationCounter INTEGER NOT NULL,
  data BLOB(2147483647),
  size BIGINT NOT NULL,
  mimeType VARCHAR(255),
  PRIMARY KEY (ID)
);

-- *** RevInfo (Commit log for envers audit trail) ***
CREATE TABLE RevInfo(
  id BIGINT NOT NULL GENERATED BY DEFAULT AS IDENTITY (START WITH 1),
  timestamp BIGINT NOT NULL,
  userLogin VARCHAR(255)
);