package holidays.holidaymanagement.dataaccess.api.dao;

import holidays.general.dataaccess.api.dao.ApplicationDao;
import holidays.holidaymanagement.dataaccess.api.HolidayTypeEntity;
import holidays.holidaymanagement.logic.api.to.HolidayTypeSearchCriteriaTo;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * Data access interface for HolidayType entities
 */
public interface HolidayTypeDao extends ApplicationDao<HolidayTypeEntity> {

  /**
   * Finds the {@link HolidayTypeEntity holidaytypes} matching the given {@link HolidayTypeSearchCriteriaTo}.
   *
   * @param criteria is the {@link HolidayTypeSearchCriteriaTo}.
   * @return the {@link PaginatedListTo} with the matching {@link HolidayTypeEntity} objects.
   */
  PaginatedListTo<HolidayTypeEntity> findHolidayTypes(HolidayTypeSearchCriteriaTo criteria);

}
