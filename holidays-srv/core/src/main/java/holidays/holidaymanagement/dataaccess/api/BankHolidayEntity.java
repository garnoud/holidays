package holidays.holidaymanagement.dataaccess.api;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import holidays.general.dataaccess.api.ApplicationPersistenceEntity;
import holidays.holidaymanagement.common.api.BankHoliday;

/**
 * @author GARNOUD
 */
@Entity
@Table(name = "bankHoliday")
public class BankHolidayEntity extends ApplicationPersistenceEntity implements BankHoliday {

  private Date beginDate;

  private Date endDate;

  private HolidayTypeEntity holidayType;

  private static final long serialVersionUID = 1L;

  /**
   * @return beginDate
   */
  @Column(name = "beginDate")
  public Date getBeginDate() {

    return this.beginDate;
  }

  /**
   * @param beginDate new value of {@link #getbeginDate}.
   */
  public void setBeginDate(Date beginDate) {

    this.beginDate = beginDate;
  }

  /**
   * @return endDate
   */
  @Column(name = "endDate")
  public Date getEndDate() {

    return this.endDate;
  }

  /**
   * @param endDate new value of {@link #getendDate}.
   */
  public void setEndDate(Date endDate) {

    this.endDate = endDate;
  }

  /**
   * @return holidayType
   */
  @ManyToOne
  @JoinColumn(name = "holidayTypeId")
  public HolidayTypeEntity getHolidayType() {

    return this.holidayType;
  }

  /**
   * @param holidayType new value of {@link #getholidayType}.
   */
  public void setHolidayType(HolidayTypeEntity holidayType) {

    this.holidayType = holidayType;
  }

  @Override
  @Transient
  public Long getHolidayTypeId() {

    if (this.holidayType == null) {
      return null;
    }
    return this.holidayType.getId();
  }

  @Override
  public void setHolidayTypeId(Long holidayTypeId) {

    if (holidayTypeId == null) {
      this.holidayType = null;
    } else {
      HolidayTypeEntity holidayTypeEntity = new HolidayTypeEntity();
      holidayTypeEntity.setId(holidayTypeId);
      this.holidayType = holidayTypeEntity;
    }
  }

}
