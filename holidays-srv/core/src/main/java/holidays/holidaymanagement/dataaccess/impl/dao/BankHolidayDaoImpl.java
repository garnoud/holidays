package holidays.holidaymanagement.dataaccess.impl.dao;

import java.util.Date;

import javax.inject.Named;

import com.mysema.query.alias.Alias;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.EntityPathBase;

import holidays.general.dataaccess.base.dao.ApplicationDaoImpl;
import holidays.holidaymanagement.dataaccess.api.BankHolidayEntity;
import holidays.holidaymanagement.dataaccess.api.dao.BankHolidayDao;
import holidays.holidaymanagement.logic.api.to.BankHolidaySearchCriteriaTo;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * This is the implementation of {@link BankHolidayDao}.
 */
@Named
public class BankHolidayDaoImpl extends ApplicationDaoImpl<BankHolidayEntity> implements BankHolidayDao {

  /**
   * The constructor.
   */
  public BankHolidayDaoImpl() {

    super();
  }

  @Override
  public Class<BankHolidayEntity> getEntityClass() {

    return BankHolidayEntity.class;
  }

  @Override
  public PaginatedListTo<BankHolidayEntity> findBankHolidays(BankHolidaySearchCriteriaTo criteria) {

    BankHolidayEntity bankholiday = Alias.alias(BankHolidayEntity.class);
    EntityPathBase<BankHolidayEntity> alias = Alias.$(bankholiday);
    JPAQuery query = new JPAQuery(getEntityManager()).from(alias);

    Date beginDate = criteria.getBeginDate();
    if (beginDate != null) {
      query.where(Alias.$(bankholiday.getBeginDate()).eq(beginDate));
    }
    Date endDate = criteria.getEndDate();
    if (endDate != null) {
      query.where(Alias.$(bankholiday.getEndDate()).eq(endDate));
    }
    Long holidayType = criteria.getHolidayTypeId();
    if (holidayType != null) {
      if (bankholiday.getHolidayType() != null) {
        query.where(Alias.$(bankholiday.getHolidayType().getId()).eq(holidayType));
      }
    }
    return findPaginated(criteria, query, alias);
  }

}