package holidays.holidaymanagement.dataaccess.api.dao;

import holidays.general.dataaccess.api.dao.ApplicationDao;
import holidays.holidaymanagement.dataaccess.api.BankHolidayEntity;
import holidays.holidaymanagement.logic.api.to.BankHolidaySearchCriteriaTo;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * Data access interface for BankHoliday entities
 */
public interface BankHolidayDao extends ApplicationDao<BankHolidayEntity> {

  /**
   * Finds the {@link BankHolidayEntity bankholidays} matching the given {@link BankHolidaySearchCriteriaTo}.
   *
   * @param criteria is the {@link BankHolidaySearchCriteriaTo}.
   * @return the {@link PaginatedListTo} with the matching {@link BankHolidayEntity} objects.
   */
  PaginatedListTo<BankHolidayEntity> findBankHolidays(BankHolidaySearchCriteriaTo criteria);
}
