package holidays.holidaymanagement.dataaccess.impl.dao;

import javax.inject.Named;

import com.mysema.query.alias.Alias;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.EntityPathBase;

import holidays.general.dataaccess.base.dao.ApplicationDaoImpl;
import holidays.holidaymanagement.dataaccess.api.HolidayTypeEntity;
import holidays.holidaymanagement.dataaccess.api.dao.HolidayTypeDao;
import holidays.holidaymanagement.logic.api.to.HolidayTypeSearchCriteriaTo;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * This is the implementation of {@link HolidayTypeDao}.
 */
@Named
public class HolidayTypeDaoImpl extends ApplicationDaoImpl<HolidayTypeEntity> implements HolidayTypeDao {

  /**
   * The constructor.
   */
  public HolidayTypeDaoImpl() {

    super();
  }

  @Override
  public Class<HolidayTypeEntity> getEntityClass() {

    return HolidayTypeEntity.class;
  }

  @Override
  public PaginatedListTo<HolidayTypeEntity> findHolidayTypes(HolidayTypeSearchCriteriaTo criteria) {

    HolidayTypeEntity holidaytype = Alias.alias(HolidayTypeEntity.class);
    EntityPathBase<HolidayTypeEntity> alias = Alias.$(holidaytype);
    JPAQuery query = new JPAQuery(getEntityManager()).from(alias);

    String name = criteria.getName();
    if (name != null) {
      query.where(Alias.$(holidaytype.getName()).eq(name));
    }
    Boolean writeable = criteria.getWriteable();
    if (writeable != null) {
      query.where(Alias.$(holidaytype.getWriteable()).eq(writeable));
    }
    return findPaginated(criteria, query, alias);
  }

}
