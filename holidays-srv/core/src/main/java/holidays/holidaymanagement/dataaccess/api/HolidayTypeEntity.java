package holidays.holidaymanagement.dataaccess.api;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import holidays.general.dataaccess.api.ApplicationPersistenceEntity;
import holidays.holidaymanagement.common.api.HolidayType;

/**
 * @author GARNOUD
 */
@Entity
@Table(name = "holidayType")
public class HolidayTypeEntity extends ApplicationPersistenceEntity implements HolidayType {

  private String name;

  private Boolean writeable;

  private static final long serialVersionUID = 1L;

  /**
   * @return name
   */
  @Column(name = "name")
  public String getName() {

    return this.name;
  }

  /**
   * @param name new value of {@link #getname}.
   */
  public void setName(String name) {

    this.name = name;
  }

  /**
   * @return writeable
   */
  @Column(name = "writeable")
  public Boolean getWriteable() {

    return this.writeable;
  }

  /**
   * @param writeable new value of {@link #getwriteable}.
   */
  public void setWriteable(Boolean writeable) {

    this.writeable = writeable;
  }

}
