package holidays.holidaymanagement.service.impl.rest;

import javax.inject.Inject;
import javax.inject.Named;

import holidays.holidaymanagement.logic.api.Holidaymanagement;
import holidays.holidaymanagement.logic.api.to.BankHolidayEto;
import holidays.holidaymanagement.logic.api.to.BankHolidaySearchCriteriaTo;
import holidays.holidaymanagement.logic.api.to.HolidayTypeEto;
import holidays.holidaymanagement.logic.api.to.HolidayTypeSearchCriteriaTo;
import holidays.holidaymanagement.service.api.rest.HolidaymanagementRestService;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * The service implementation for REST calls in order to execute the logic of component {@link Holidaymanagement}.
 */
@Named("HolidaymanagementRestService")
public class HolidaymanagementRestServiceImpl implements HolidaymanagementRestService {

  @Inject
  private Holidaymanagement holidaymanagement;

  @Override
  public HolidayTypeEto getHolidayType(long id) {

    return this.holidaymanagement.findHolidayType(id);
  }

  @Override
  public HolidayTypeEto saveHolidayType(HolidayTypeEto holidaytype) {

    return this.holidaymanagement.saveHolidayType(holidaytype);
  }

  @Override
  public void deleteHolidayType(long id) {

    this.holidaymanagement.deleteHolidayType(id);
  }

  @Override
  public PaginatedListTo<HolidayTypeEto> findHolidayTypesByPost(HolidayTypeSearchCriteriaTo searchCriteriaTo) {

    return this.holidaymanagement.findHolidayTypeEtos(searchCriteriaTo);
  }

  @Override
  public BankHolidayEto getBankHoliday(long id) {

    return this.holidaymanagement.findBankHoliday(id);
  }

  @Override
  public BankHolidayEto saveBankHoliday(BankHolidayEto bankholiday) {

    return this.holidaymanagement.saveBankHoliday(bankholiday);
  }

  @Override
  public void deleteBankHoliday(long id) {

    this.holidaymanagement.deleteBankHoliday(id);
  }

  @Override
  public PaginatedListTo<BankHolidayEto> findBankHolidaysByPost(BankHolidaySearchCriteriaTo searchCriteriaTo) {

    return this.holidaymanagement.findBankHolidayEtos(searchCriteriaTo);
  }

}
