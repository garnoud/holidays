package holidays.holidaymanagement.service.api.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import holidays.holidaymanagement.logic.api.Holidaymanagement;
import holidays.holidaymanagement.logic.api.to.BankHolidayEto;
import holidays.holidaymanagement.logic.api.to.BankHolidaySearchCriteriaTo;
import holidays.holidaymanagement.logic.api.to.HolidayTypeEto;
import holidays.holidaymanagement.logic.api.to.HolidayTypeSearchCriteriaTo;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * The service interface for REST calls in order to execute the logic of component {@link Holidaymanagement}.
 */
@Path("/holidaymanagement/v1")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface HolidaymanagementRestService {

  /**
   * Delegates to {@link Holidaymanagement#findHolidayType}.
   *
   * @param id the ID of the {@link HolidayTypeEto}
   * @return the {@link HolidayTypeEto}
   */
  @GET
  @Path("/holidaytype/{id}/")
  public HolidayTypeEto getHolidayType(@PathParam("id") long id);

  /**
   * Delegates to {@link Holidaymanagement#saveHolidayType}.
   *
   * @param holidaytype the {@link HolidayTypeEto} to be saved
   * @return the recently created {@link HolidayTypeEto}
   */
  @POST
  @Path("/holidaytype/")
  public HolidayTypeEto saveHolidayType(HolidayTypeEto holidaytype);

  /**
   * Delegates to {@link Holidaymanagement#deleteHolidayType}.
   *
   * @param id ID of the {@link HolidayTypeEto} to be deleted
   */
  @DELETE
  @Path("/holidaytype/{id}/")
  public void deleteHolidayType(@PathParam("id") long id);

  /**
   * Delegates to {@link Holidaymanagement#findHolidayTypeEtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding holidaytypes.
   * @return the {@link PaginatedListTo list} of matching {@link HolidayTypeEto}s.
   */
  @Path("/holidaytype/search")
  @POST
  public PaginatedListTo<HolidayTypeEto> findHolidayTypesByPost(HolidayTypeSearchCriteriaTo searchCriteriaTo);

  /**
   * Delegates to {@link Holidaymanagement#findBankHoliday}.
   *
   * @param id the ID of the {@link BankHolidayEto}
   * @return the {@link BankHolidayEto}
   */
  @GET
  @Path("/bankholiday/{id}/")
  public BankHolidayEto getBankHoliday(@PathParam("id") long id);

  /**
   * Delegates to {@link Holidaymanagement#saveBankHoliday}.
   *
   * @param bankholiday the {@link BankHolidayEto} to be saved
   * @return the recently created {@link BankHolidayEto}
   */
  @POST
  @Path("/bankholiday/")
  public BankHolidayEto saveBankHoliday(BankHolidayEto bankholiday);

  /**
   * Delegates to {@link Holidaymanagement#deleteBankHoliday}.
   *
   * @param id ID of the {@link BankHolidayEto} to be deleted
   */
  @DELETE
  @Path("/bankholiday/{id}/")
  public void deleteBankHoliday(@PathParam("id") long id);

  /**
   * Delegates to {@link Holidaymanagement#findBankHolidayEtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding bankholidays.
   * @return the {@link PaginatedListTo list} of matching {@link BankHolidayEto}s.
   */
  @Path("/bankholiday/search")
  @POST
  public PaginatedListTo<BankHolidayEto> findBankHolidaysByPost(BankHolidaySearchCriteriaTo searchCriteriaTo);

}
