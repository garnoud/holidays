package holidays.holidaymanagement.common.api;

import java.util.Date;

import holidays.general.common.api.ApplicationEntity;

public interface BankHoliday extends ApplicationEntity {

  public Date getBeginDate();

  public void setBeginDate(Date beginDate);

  public Date getEndDate();

  public void setEndDate(Date endDate);

  public Long getHolidayTypeId();

  public void setHolidayTypeId(Long holidayTypeId);

}
