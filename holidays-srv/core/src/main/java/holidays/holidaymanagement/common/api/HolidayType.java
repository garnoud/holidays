package holidays.holidaymanagement.common.api;

import holidays.general.common.api.ApplicationEntity;

public interface HolidayType extends ApplicationEntity {

  public String getName();

  public void setName(String name);

  public Boolean getWriteable();

  public void setWriteable(Boolean writeable);

}
