package holidays.holidaymanagement.logic.api.usecase;

import holidays.holidaymanagement.logic.api.to.BankHolidayEto;

/**
 * Interface of UcManageBankHoliday to centralize documentation and signatures of methods.
 */
public interface UcManageBankHoliday {

  /**
   * Deletes a bankHoliday from the database by its id 'bankHolidayId'.
   *
   * @param bankHolidayId Id of the bankHoliday to delete
   * @return boolean <code>true</code> if the bankHoliday can be deleted, <code>false</code> otherwise
   */
  boolean deleteBankHoliday(Long bankHolidayId);

  /**
   * Saves a bankHoliday and store it in the database.
   *
   * @param bankHoliday the {@link BankHolidayEto} to create.
   * @return the new {@link BankHolidayEto} that has been saved with ID and version.
   */
  BankHolidayEto saveBankHoliday(BankHolidayEto bankHoliday);

}
