package holidays.holidaymanagement.logic.api.usecase;

import java.util.List;

import holidays.holidaymanagement.logic.api.to.BankHolidayEto;
import holidays.holidaymanagement.logic.api.to.BankHolidaySearchCriteriaTo;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

public interface UcFindBankHoliday {

  /**
   * Returns a BankHoliday by its id 'id'.
   *
   * @param id The id 'id' of the BankHoliday.
   * @return The {@link BankHolidayEto} with id 'id'
   */
  BankHolidayEto findBankHoliday(Long id);

  /**
   * Returns a paginated list of BankHolidays matching the search criteria.
   *
   * @param criteria the {@link BankHolidaySearchCriteriaTo}.
   * @return the {@link List} of matching {@link BankHolidayEto}s.
   */
  PaginatedListTo<BankHolidayEto> findBankHolidayEtos(BankHolidaySearchCriteriaTo criteria);

}
