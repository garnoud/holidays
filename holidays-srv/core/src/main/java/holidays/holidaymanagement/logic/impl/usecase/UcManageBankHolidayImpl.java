package holidays.holidaymanagement.logic.impl.usecase;

import java.util.Objects;

import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import holidays.general.logic.api.UseCase;
import holidays.holidaymanagement.dataaccess.api.BankHolidayEntity;
import holidays.holidaymanagement.logic.api.to.BankHolidayEto;
import holidays.holidaymanagement.logic.api.usecase.UcManageBankHoliday;
import holidays.holidaymanagement.logic.base.usecase.AbstractBankHolidayUc;

/**
 * Use case implementation for modifying and deleting BankHolidays
 */
@Named
@UseCase
@Validated
@Transactional
public class UcManageBankHolidayImpl extends AbstractBankHolidayUc implements UcManageBankHoliday {

  /** Logger instance. */
  private static final Logger LOG = LoggerFactory.getLogger(UcManageBankHolidayImpl.class);

  @Override
  public boolean deleteBankHoliday(Long bankHolidayId) {

    BankHolidayEntity bankHoliday = getBankHolidayDao().find(bankHolidayId);
    getBankHolidayDao().delete(bankHoliday);
    LOG.debug("The bankHoliday with id '{}' has been deleted.", bankHolidayId);
    return true;
  }

  @Override
  public BankHolidayEto saveBankHoliday(BankHolidayEto bankHoliday) {

    Objects.requireNonNull(bankHoliday, "bankHoliday");

    BankHolidayEntity bankHolidayEntity = getBeanMapper().map(bankHoliday, BankHolidayEntity.class);

    // initialize, validate bankHolidayEntity here if necessary
    BankHolidayEntity resultEntity = getBankHolidayDao().save(bankHolidayEntity);
    LOG.debug("BankHoliday with id '{}' has been created.", resultEntity.getId());
    return getBeanMapper().map(resultEntity, BankHolidayEto.class);
  }
}
