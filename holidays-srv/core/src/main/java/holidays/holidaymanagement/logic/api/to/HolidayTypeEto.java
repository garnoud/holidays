package holidays.holidaymanagement.logic.api.to;

import holidays.general.common.api.to.AbstractEto;
import holidays.holidaymanagement.common.api.HolidayType;

/**
 * Entity transport object of HolidayType
 */
public class HolidayTypeEto extends AbstractEto implements HolidayType {

  private static final long serialVersionUID = 1L;

  private String name;

  private Boolean writeable;

  @Override
  public String getName() {

    return name;
  }

  @Override
  public void setName(String name) {

    this.name = name;
  }

  @Override
  public Boolean getWriteable() {

    return writeable;
  }

  @Override
  public void setWriteable(Boolean writeable) {

    this.writeable = writeable;
  }

  @Override
  public int hashCode() {

    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
    result = prime * result + ((this.writeable == null) ? 0 : this.writeable.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {

    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    // class check will be done by super type EntityTo!
    if (!super.equals(obj)) {
      return false;
    }
    HolidayTypeEto other = (HolidayTypeEto) obj;
    if (this.name == null) {
      if (other.name != null) {
        return false;
      }
    } else if (!this.name.equals(other.name)) {
      return false;
    }
    if (this.writeable == null) {
      if (other.writeable != null) {
        return false;
      }
    } else if (!this.writeable.equals(other.writeable)) {
      return false;
    }
    return true;
  }

}
