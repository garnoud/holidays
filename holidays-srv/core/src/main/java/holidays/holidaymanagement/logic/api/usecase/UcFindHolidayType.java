package holidays.holidaymanagement.logic.api.usecase;

import java.util.List;

import holidays.holidaymanagement.logic.api.to.HolidayTypeEto;
import holidays.holidaymanagement.logic.api.to.HolidayTypeSearchCriteriaTo;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

public interface UcFindHolidayType {

  /**
   * Returns a HolidayType by its id 'id'.
   *
   * @param id The id 'id' of the HolidayType.
   * @return The {@link HolidayTypeEto} with id 'id'
   */
  HolidayTypeEto findHolidayType(Long id);

  /**
   * Returns a paginated list of HolidayTypes matching the search criteria.
   *
   * @param criteria the {@link HolidayTypeSearchCriteriaTo}.
   * @return the {@link List} of matching {@link HolidayTypeEto}s.
   */
  PaginatedListTo<HolidayTypeEto> findHolidayTypeEtos(HolidayTypeSearchCriteriaTo criteria);

}
