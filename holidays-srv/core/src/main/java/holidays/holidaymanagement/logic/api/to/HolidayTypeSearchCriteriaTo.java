package holidays.holidaymanagement.logic.api.to;

import io.oasp.module.jpa.common.api.to.SearchCriteriaTo;

/**
 * This is the {@link SearchCriteriaTo search criteria} {@link net.sf.mmm.util.transferobject.api.TransferObject TO}
 * used to find {@link holidays.holidaymanagement.common.api.HolidayType}s.
 */
public class HolidayTypeSearchCriteriaTo extends SearchCriteriaTo {

  private static final long serialVersionUID = 1L;

  private String name;

  private Boolean writeable;

  /**
   * The constructor.
   */
  public HolidayTypeSearchCriteriaTo() {

    super();
  }

  public String getName() {

    return name;
  }

  public void setName(String name) {

    this.name = name;
  }

  public Boolean getWriteable() {

    return writeable;
  }

  public void setWriteable(Boolean writeable) {

    this.writeable = writeable;
  }

}
