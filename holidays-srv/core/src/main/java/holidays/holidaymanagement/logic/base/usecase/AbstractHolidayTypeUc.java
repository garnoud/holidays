package holidays.holidaymanagement.logic.base.usecase;

import javax.inject.Inject;

import holidays.general.logic.base.AbstractUc;
import holidays.holidaymanagement.dataaccess.api.dao.HolidayTypeDao;

/**
 * Abstract use case for HolidayTypes, which provides access to the commonly necessary data access objects.
 */
public class AbstractHolidayTypeUc extends AbstractUc {

  /** @see #getHolidayTypeDao() */
  @Inject
  private HolidayTypeDao holidayTypeDao;

  /**
   * Returns the field 'holidayTypeDao'.
   * 
   * @return the {@link HolidayTypeDao} instance.
   */
  public HolidayTypeDao getHolidayTypeDao() {

    return this.holidayTypeDao;
  }

}
