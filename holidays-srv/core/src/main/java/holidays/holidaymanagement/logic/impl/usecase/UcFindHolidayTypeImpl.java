package holidays.holidaymanagement.logic.impl.usecase;

import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import holidays.general.logic.api.UseCase;
import holidays.holidaymanagement.dataaccess.api.HolidayTypeEntity;
import holidays.holidaymanagement.logic.api.to.HolidayTypeEto;
import holidays.holidaymanagement.logic.api.to.HolidayTypeSearchCriteriaTo;
import holidays.holidaymanagement.logic.api.usecase.UcFindHolidayType;
import holidays.holidaymanagement.logic.base.usecase.AbstractHolidayTypeUc;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * Use case implementation for searching, filtering and getting HolidayTypes
 */
@Named
@UseCase
@Validated
@Transactional
public class UcFindHolidayTypeImpl extends AbstractHolidayTypeUc implements UcFindHolidayType {

  /**
   * Logger instance.
   */
  private static final Logger LOG = LoggerFactory.getLogger(UcFindHolidayTypeImpl.class);

  @Override
  public HolidayTypeEto findHolidayType(Long id) {

    LOG.debug("Get HolidayType with id {} from database.", id);
    return getBeanMapper().map(getHolidayTypeDao().findOne(id), HolidayTypeEto.class);
  }

  @Override
  public PaginatedListTo<HolidayTypeEto> findHolidayTypeEtos(HolidayTypeSearchCriteriaTo criteria) {

    criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
    PaginatedListTo<HolidayTypeEntity> holidaytypes = getHolidayTypeDao().findHolidayTypes(criteria);
    return mapPaginatedEntityList(holidaytypes, HolidayTypeEto.class);
  }

}
