package holidays.holidaymanagement.logic.impl.usecase;

import java.util.Objects;

import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import holidays.general.logic.api.UseCase;
import holidays.holidaymanagement.dataaccess.api.HolidayTypeEntity;
import holidays.holidaymanagement.logic.api.to.HolidayTypeEto;
import holidays.holidaymanagement.logic.api.usecase.UcManageHolidayType;
import holidays.holidaymanagement.logic.base.usecase.AbstractHolidayTypeUc;

/**
 * Use case implementation for modifying and deleting HolidayTypes
 */
@Named
@UseCase
@Validated
@Transactional
public class UcManageHolidayTypeImpl extends AbstractHolidayTypeUc implements UcManageHolidayType {

  /**
   * Logger instance.
   */
  private static final Logger LOG = LoggerFactory.getLogger(UcManageHolidayTypeImpl.class);

  @Override
  public boolean deleteHolidayType(Long holidayTypeId) {

    HolidayTypeEntity holidayType = getHolidayTypeDao().find(holidayTypeId);
    getHolidayTypeDao().delete(holidayType);
    LOG.debug("The holidayType with id '{}' has been deleted.", holidayTypeId);
    return true;
  }

  @Override
  public HolidayTypeEto saveHolidayType(HolidayTypeEto holidayType) {

    Objects.requireNonNull(holidayType, "holidayType");

    HolidayTypeEntity holidayTypeEntity = getBeanMapper().map(holidayType, HolidayTypeEntity.class);

    // initialize, validate holidayTypeEntity here if necessary
    HolidayTypeEntity resultEntity = getHolidayTypeDao().save(holidayTypeEntity);
    LOG.debug("HolidayType with id '{}' has been created.", resultEntity.getId());
    return getBeanMapper().map(resultEntity, HolidayTypeEto.class);
  }

}
