package holidays.holidaymanagement.logic.api.to;

import holidays.general.common.api.to.AbstractCto;

/**
 * Composite transport object of HolidayType
 */
public class HolidayTypeCto extends AbstractCto {

  private static final long serialVersionUID = 1L;

  private HolidayTypeEto holidayType;

  public HolidayTypeEto getHolidayType() {

    return holidayType;
  }

  public void setHolidayType(HolidayTypeEto holidayType) {

    this.holidayType = holidayType;
  }

}
