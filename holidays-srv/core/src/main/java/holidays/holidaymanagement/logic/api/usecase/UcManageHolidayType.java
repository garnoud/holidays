package holidays.holidaymanagement.logic.api.usecase;

import holidays.holidaymanagement.logic.api.to.HolidayTypeEto;

/**
 * Interface of UcManageHolidayType to centralize documentation and signatures of methods.
 */
public interface UcManageHolidayType {

  /**
   * Deletes a holidayType from the database by its id 'holidayTypeId'.
   *
   * @param holidayTypeId Id of the holidayType to delete
   * @return boolean <code>true</code> if the holidayType can be deleted, <code>false</code> otherwise
   */
  boolean deleteHolidayType(Long holidayTypeId);

  /**
   * Saves a holidayType and store it in the database.
   *
   * @param holidayType the {@link HolidayTypeEto} to create.
   * @return the new {@link HolidayTypeEto} that has been saved with ID and version.
   */
  HolidayTypeEto saveHolidayType(HolidayTypeEto holidayType);

}
