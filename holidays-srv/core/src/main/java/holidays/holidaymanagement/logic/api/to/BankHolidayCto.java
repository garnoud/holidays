package holidays.holidaymanagement.logic.api.to;

import holidays.general.common.api.to.AbstractCto;

/**
 * Composite transport object of BankHoliday
 */
public class BankHolidayCto extends AbstractCto {

  private static final long serialVersionUID = 1L;

  private BankHolidayEto bankHoliday;

  private HolidayTypeCto holidayType;

  public BankHolidayEto getBankHoliday() {

    return bankHoliday;
  }

  public void setBankHoliday(BankHolidayEto bankHoliday) {

    this.bankHoliday = bankHoliday;
  }

  public HolidayTypeCto getHolidayType() {

    return holidayType;
  }

  public void setHolidayType(HolidayTypeCto holidayType) {

    this.holidayType = holidayType;
  }

  @Override
  public int hashCode() {

    return this.bankHoliday.hashCode();
  }

  @Override
  public boolean equals(Object obj) {

    return this.bankHoliday.equals(obj);
  }

}
