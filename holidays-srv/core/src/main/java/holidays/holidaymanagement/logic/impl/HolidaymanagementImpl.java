package holidays.holidaymanagement.logic.impl;

import java.util.Objects;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import holidays.general.logic.base.AbstractComponentFacade;
import holidays.holidaymanagement.dataaccess.api.BankHolidayEntity;
import holidays.holidaymanagement.dataaccess.api.dao.BankHolidayDao;
import holidays.holidaymanagement.logic.api.Holidaymanagement;
import holidays.holidaymanagement.logic.api.to.BankHolidayCto;
import holidays.holidaymanagement.logic.api.to.BankHolidayEto;
import holidays.holidaymanagement.logic.api.to.BankHolidaySearchCriteriaTo;
import holidays.holidaymanagement.logic.api.to.HolidayTypeEto;
import holidays.holidaymanagement.logic.api.to.HolidayTypeSearchCriteriaTo;
import holidays.holidaymanagement.logic.api.usecase.UcFindBankHoliday;
import holidays.holidaymanagement.logic.api.usecase.UcFindHolidayType;
import holidays.holidaymanagement.logic.api.usecase.UcManageBankHoliday;
import holidays.holidaymanagement.logic.api.usecase.UcManageHolidayType;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * Implementation of component interface of holidaymanagement
 */
@Transactional
@Named
public class HolidaymanagementImpl extends AbstractComponentFacade implements Holidaymanagement {

  /**
   * Logger instance.
   */
  private static final Logger LOG = LoggerFactory.getLogger(HolidaymanagementImpl.class);

  /**
   * @see #getBankHolidayDao()
   */
  @Inject
  private BankHolidayDao bankHolidayDao;

  @Inject
  private UcFindBankHoliday ucFindBankHoliday;

  @Inject
  private UcManageBankHoliday ucManageBankHoliday;

  @Inject
  private UcFindHolidayType ucFindHolidayType;

  @Inject
  private UcManageHolidayType ucManageHolidayType;

  /**
   * The constructor.
   */
  public HolidaymanagementImpl() {

    super();
  }

  @Override
  public BankHolidayEto findBankHoliday(Long id) {

    LOG.debug("Get BankHoliday with id {} from database.", id);
    return getBeanMapper().map(getBankHolidayDao().findOne(id), BankHolidayEto.class);
  }

  @Override
  public PaginatedListTo<BankHolidayEto> findBankHolidayEtos(BankHolidaySearchCriteriaTo criteria) {

    criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
    PaginatedListTo<BankHolidayEntity> bankholidays = getBankHolidayDao().findBankHolidays(criteria);
    return mapPaginatedEntityList(bankholidays, BankHolidayEto.class);
  }

  @Override
  public boolean deleteBankHoliday(Long bankHolidayId) {

    BankHolidayEntity bankHoliday = getBankHolidayDao().find(bankHolidayId);
    getBankHolidayDao().delete(bankHoliday);
    LOG.debug("The bankHoliday with id '{}' has been deleted.", bankHolidayId);
    return true;
  }

  @Override
  public BankHolidayEto saveBankHoliday(BankHolidayEto bankHoliday) {

    Objects.requireNonNull(bankHoliday, "bankHoliday");
    BankHolidayEntity bankHolidayEntity = getBeanMapper().map(bankHoliday, BankHolidayEntity.class);

    // initialize, validate bankHolidayEntity here if necessary
    getBankHolidayDao().save(bankHolidayEntity);
    LOG.debug("BankHoliday with id '{}' has been created.", bankHolidayEntity.getId());

    return getBeanMapper().map(bankHolidayEntity, BankHolidayEto.class);
  }

  /**
   * Returns the field 'bankHolidayDao'.
   *
   * @return the {@link BankHolidayDao} instance.
   */
  public BankHolidayDao getBankHolidayDao() {

    return this.bankHolidayDao;
  }

  @Override
  public BankHolidayCto findBankHolidayCto(Long id) {

    BankHolidayCto bankHolidayCto = new BankHolidayCto();
    BankHolidayEto bankHolidayEto = this.findBankHoliday(id);
    bankHolidayCto.setBankHoliday(bankHolidayEto);
    LOG.debug("Cto created for id ", id);
    return bankHolidayCto;
  }

  @Override
  public HolidayTypeEto findHolidayType(Long id) {

    return this.ucFindHolidayType.findHolidayType(id);
  }

  @Override
  public PaginatedListTo<HolidayTypeEto> findHolidayTypeEtos(HolidayTypeSearchCriteriaTo criteria) {

    return this.ucFindHolidayType.findHolidayTypeEtos(criteria);
  }

  @Override
  public HolidayTypeEto saveHolidayType(HolidayTypeEto holidaytype) {

    return this.ucManageHolidayType.saveHolidayType(holidaytype);
  }

  @Override
  public boolean deleteHolidayType(Long id) {

    return this.ucManageHolidayType.deleteHolidayType(id);
  }

}
