package holidays.holidaymanagement.logic.api;

import holidays.holidaymanagement.logic.api.to.BankHolidayCto;
import holidays.holidaymanagement.logic.api.to.BankHolidayEto;
import holidays.holidaymanagement.logic.api.to.BankHolidaySearchCriteriaTo;
import holidays.holidaymanagement.logic.api.usecase.UcFindBankHoliday;
import holidays.holidaymanagement.logic.api.usecase.UcFindHolidayType;
import holidays.holidaymanagement.logic.api.usecase.UcManageBankHoliday;
import holidays.holidaymanagement.logic.api.usecase.UcManageHolidayType;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * Interface for Holidaymanagement component.
 */
public interface Holidaymanagement
    extends UcFindBankHoliday, UcManageBankHoliday, UcFindHolidayType, UcManageHolidayType {

  /**
   * Returns a BankHoliday by its id 'id'.
   *
   * @param id The id 'id' of the BankHoliday.
   * @return The {@link BankHolidayEto} with id 'id'
   */
  BankHolidayEto findBankHoliday(Long id);

  /**
   * Returns a paginated list of BankHolidays matching the search criteria.
   *
   * @param criteria the {@link BankHolidaySearchCriteriaTo}.
   * @return the {@link List} of matching {@link BankHolidayEto}s.
   */
  PaginatedListTo<BankHolidayEto> findBankHolidayEtos(BankHolidaySearchCriteriaTo criteria);

  /**
   * Deletes a bankHoliday from the database by its id 'bankHolidayId'.
   *
   * @param bankHolidayId Id of the bankHoliday to delete
   * @return boolean <code>true</code> if the bankHoliday can be deleted, <code>false</code> otherwise
   */
  boolean deleteBankHoliday(Long bankHolidayId);

  /**
   * Saves a bankHoliday and store it in the database.
   *
   * @param bankHoliday the {@link BankHolidayEto} to create.
   * @return the new {@link BankHolidayEto} that has been saved with ID and version.
   */
  BankHolidayEto saveBankHoliday(BankHolidayEto bankHoliday);

  BankHolidayCto findBankHolidayCto(Long id);

}
