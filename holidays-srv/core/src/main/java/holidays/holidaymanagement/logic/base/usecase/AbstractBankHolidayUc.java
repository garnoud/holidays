package holidays.holidaymanagement.logic.base.usecase;

import javax.inject.Inject;

import holidays.general.logic.base.AbstractUc;
import holidays.holidaymanagement.dataaccess.api.dao.BankHolidayDao;

/**
 * Abstract use case for BankHolidays, which provides access to the commonly necessary data access objects.
 */
public class AbstractBankHolidayUc extends AbstractUc {

  /** @see #getBankHolidayDao() */
  @Inject
  private BankHolidayDao bankHolidayDao;

  /**
   * Returns the field 'bankHolidayDao'.
   * 
   * @return the {@link BankHolidayDao} instance.
   */
  public BankHolidayDao getBankHolidayDao() {

    return this.bankHolidayDao;
  }

}
