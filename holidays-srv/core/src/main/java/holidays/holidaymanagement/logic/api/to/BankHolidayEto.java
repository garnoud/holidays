package holidays.holidaymanagement.logic.api.to;

import java.util.Date;

import holidays.general.common.api.to.AbstractEto;
import holidays.holidaymanagement.common.api.BankHoliday;

/**
 * Entity transport object of BankHoliday
 */
public class BankHolidayEto extends AbstractEto implements BankHoliday {

  private static final long serialVersionUID = 1L;

  private Date beginDate;

  private Date endDate;

  private Long holidayTypeId;

  @Override
  public Date getBeginDate() {

    return this.beginDate;
  }

  @Override
  public void setBeginDate(Date beginDate) {

    this.beginDate = beginDate;
  }

  @Override
  public Date getEndDate() {

    return this.endDate;
  }

  @Override
  public void setEndDate(Date endDate) {

    this.endDate = endDate;
  }

  @Override
  public Long getHolidayTypeId() {

    return this.holidayTypeId;
  }

  @Override
  public void setHolidayTypeId(Long holidayTypeId) {

    this.holidayTypeId = holidayTypeId;
  }

  @Override
  public int hashCode() {

    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((this.beginDate == null) ? 0 : this.beginDate.hashCode());
    result = prime * result + ((this.endDate == null) ? 0 : this.endDate.hashCode());

    result = prime * result + ((this.holidayTypeId == null) ? 0 : this.holidayTypeId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {

    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    // class check will be done by super type EntityTo!
    if (!super.equals(obj)) {
      return false;
    }
    BankHolidayEto other = (BankHolidayEto) obj;
    if (this.beginDate == null) {
      if (other.beginDate != null) {
        return false;
      }
    } else if (!this.beginDate.equals(other.beginDate)) {
      return false;
    }
    if (this.endDate == null) {
      if (other.endDate != null) {
        return false;
      }
    } else if (!this.endDate.equals(other.endDate)) {
      return false;
    }

    if (this.holidayTypeId == null) {
      if (other.holidayTypeId != null) {
        return false;
      }
    } else if (!this.holidayTypeId.equals(other.holidayTypeId)) {
      return false;
    }
    return true;
  }
}
