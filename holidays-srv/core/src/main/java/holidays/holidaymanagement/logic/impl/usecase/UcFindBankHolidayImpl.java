package holidays.holidaymanagement.logic.impl.usecase;

import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import holidays.general.logic.api.UseCase;
import holidays.holidaymanagement.dataaccess.api.BankHolidayEntity;
import holidays.holidaymanagement.logic.api.to.BankHolidayEto;
import holidays.holidaymanagement.logic.api.to.BankHolidaySearchCriteriaTo;
import holidays.holidaymanagement.logic.api.usecase.UcFindBankHoliday;
import holidays.holidaymanagement.logic.base.usecase.AbstractBankHolidayUc;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * Use case implementation for searching, filtering and getting BankHolidays
 */
@Named
@UseCase
@Validated
@Transactional
public class UcFindBankHolidayImpl extends AbstractBankHolidayUc implements UcFindBankHoliday {

  /** Logger instance. */
  private static final Logger LOG = LoggerFactory.getLogger(UcFindBankHolidayImpl.class);

  @Override
  public BankHolidayEto findBankHoliday(Long id) {

    LOG.debug("Get BankHoliday with id {} from database.", id);
    return getBeanMapper().map(getBankHolidayDao().findOne(id), BankHolidayEto.class);
  }

  @Override
  public PaginatedListTo<BankHolidayEto> findBankHolidayEtos(BankHolidaySearchCriteriaTo criteria) {

    criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
    PaginatedListTo<BankHolidayEntity> bankholidays = getBankHolidayDao().findBankHolidays(criteria);
    return mapPaginatedEntityList(bankholidays, BankHolidayEto.class);
  }

}
