package holidays.holidaymanagement.logic.api.to;

import java.util.Date;

import io.oasp.module.jpa.common.api.to.SearchCriteriaTo;

/**
 * This is the {@link SearchCriteriaTo search criteria} {@link net.sf.mmm.util.transferobject.api.TransferObject TO}
 * used to find {@link holidays.holidaymanagement.common.api.BankHoliday}s.
 *
 */
public class BankHolidaySearchCriteriaTo extends SearchCriteriaTo {

  private static final long serialVersionUID = 1L;

  private Date beginDate;

  private Date endDate;

  private Long holidayTypeId;

  /**
   * The constructor.
   */
  public BankHolidaySearchCriteriaTo() {

    super();
  }

  public Date getBeginDate() {

    return this.beginDate;
  }

  public void setBeginDate(Date beginDate) {

    this.beginDate = beginDate;
  }

  public Date getEndDate() {

    return this.endDate;
  }

  public void setEndDate(Date endDate) {

    this.endDate = endDate;
  }

  public Long getHolidayTypeId() {

    return this.holidayTypeId;
  }

  public void setHolidayTypeId(Long holidayTypeId) {

    this.holidayTypeId = holidayTypeId;
  }

}
