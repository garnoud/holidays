package holidays.general.dataaccess.api.dao;

import holidays.general.dataaccess.api.BinaryObjectEntity;

/**
 * {@link ApplicationDao Data Access Object} for {@link BinaryObjectEntity} entity.
 *
 */
public interface BinaryObjectDao extends ApplicationDao<BinaryObjectEntity> {

}
