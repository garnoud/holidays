package holidays.usermanagement.dataaccess.api;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import holidays.general.dataaccess.api.ApplicationPersistenceEntity;

/**
 * @author GARNOUD
 *
 */
@Entity
@Table(name = "people")
public class PeopleEntity extends ApplicationPersistenceEntity {

  private String matricule;

  private String lastName;

  private String firstName;

  /**
   * @return matricule
   */
  @Column(name = "matricule")
  public String getMatricule() {

    return this.matricule;
  }

  /**
   * @param matricule new value of {@link #getmatricule}.
   */
  public void setMatricule(String matricule) {

    this.matricule = matricule;
  }

  /**
   * @return lastName
   */
  @Column(name = "lastName")
  public String getLastName() {

    return this.lastName;
  }

  /**
   * @param lastName new value of {@link #getlastName}.
   */
  public void setLastName(String lastName) {

    this.lastName = lastName;
  }

  /**
   * @return firstName
   */
  @Column(name = "firstName")
  public String getFirstName() {

    return this.firstName;
  }

  /**
   * @param firstName new value of {@link #getfirstName}.
   */
  public void setFirstName(String firstName) {

    this.firstName = firstName;
  }

}
