package holidays.usermanagement.dataaccess.api;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import holidays.general.dataaccess.api.ApplicationPersistenceEntity;

/**
 * @author GARNOUD
 *
 */
@Entity
@Table(name = "engagement")
public class EngagementEntity extends ApplicationPersistenceEntity {

  private String name;

  /**
   * @return name
   */
  @Column(name = "name")
  public String getName() {

    return this.name;
  }

  /**
   * @param name new value of {@link #getname}.
   */
  public void setName(String name) {

    this.name = name;
  }

}
