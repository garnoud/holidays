package holidays.engagementmanagement.logic.base.usecase;

import javax.inject.Inject;

import holidays.engagementmanagement.dataaccess.api.dao.EngagementDao;
import holidays.general.logic.base.AbstractUc;

/**
 * Abstract use case for Engagements, which provides access to the commonly necessary data access objects.
 */
public class AbstractEngagementUc extends AbstractUc {

  /** @see #getEngagementDao() */
  @Inject
  private EngagementDao engagementDao;

  /**
   * Returns the field 'engagementDao'.
   * 
   * @return the {@link EngagementDao} instance.
   */
  public EngagementDao getEngagementDao() {

    return this.engagementDao;
  }

}
