package holidays.engagementmanagement.logic.impl.usecase;

import java.util.Objects;

import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import holidays.engagementmanagement.dataaccess.api.EngagementEntity;
import holidays.engagementmanagement.logic.api.to.EngagementEto;
import holidays.engagementmanagement.logic.api.usecase.UcManageEngagement;
import holidays.engagementmanagement.logic.base.usecase.AbstractEngagementUc;
import holidays.general.logic.api.UseCase;

/**
 * Use case implementation for modifying and deleting Engagements
 */
@Named
@UseCase
@Validated
@Transactional
public class UcManageEngagementImpl extends AbstractEngagementUc implements UcManageEngagement {

  /** Logger instance. */
  private static final Logger LOG = LoggerFactory.getLogger(UcManageEngagementImpl.class);

  @Override
  public boolean deleteEngagement(Long engagementId) {

    EngagementEntity engagement = getEngagementDao().find(engagementId);
    getEngagementDao().delete(engagement);
    LOG.debug("The engagement with id '{}' has been deleted.", engagementId);
    return true;
  }

  @Override
  public EngagementEto saveEngagement(EngagementEto engagement) {

    Objects.requireNonNull(engagement, "engagement");

    EngagementEntity engagementEntity = getBeanMapper().map(engagement, EngagementEntity.class);

    // initialize, validate engagementEntity here if necessary
    EngagementEntity resultEntity = getEngagementDao().save(engagementEntity);
    LOG.debug("Engagement with id '{}' has been created.", resultEntity.getId());
    return getBeanMapper().map(resultEntity, EngagementEto.class);
  }
}
