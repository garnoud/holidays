package holidays.engagementmanagement.logic.api.to;

import holidays.general.common.api.to.AbstractCto;

/**
 * Composite transport object of Engagement
 */
public class EngagementCto extends AbstractCto {

  private static final long serialVersionUID = 1L;

  private EngagementEto engagement;

  public EngagementEto getEngagement() {

    return engagement;
  }

  public void setEngagement(EngagementEto engagement) {

    this.engagement = engagement;
  }

}
