package holidays.engagementmanagement.logic.api;

import holidays.engagementmanagement.logic.api.usecase.UcFindEngagement;
import holidays.engagementmanagement.logic.api.usecase.UcManageEngagement;

/**
 * Interface for Engagementmanagement component.
 */
public interface Engagementmanagement extends UcFindEngagement, UcManageEngagement {

}
