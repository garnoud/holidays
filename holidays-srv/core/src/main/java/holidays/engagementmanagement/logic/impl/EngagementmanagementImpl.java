package holidays.engagementmanagement.logic.impl;

import javax.inject.Inject;
import javax.inject.Named;

import holidays.engagementmanagement.logic.api.Engagementmanagement;
import holidays.engagementmanagement.logic.api.to.EngagementEto;
import holidays.engagementmanagement.logic.api.to.EngagementSearchCriteriaTo;
import holidays.engagementmanagement.logic.api.usecase.UcFindEngagement;
import holidays.engagementmanagement.logic.api.usecase.UcManageEngagement;
import holidays.general.logic.base.AbstractComponentFacade;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * Implementation of component interface of engagementmanagement
 */
@Named
public class EngagementmanagementImpl extends AbstractComponentFacade implements Engagementmanagement {

  @Inject
  private UcFindEngagement ucFindEngagement;

  @Inject
  private UcManageEngagement ucManageEngagement;

  /**
   * The constructor.
   */
  public EngagementmanagementImpl() {
    super();
  }

  @Override
  public EngagementEto findEngagement(Long id) {

    return this.ucFindEngagement.findEngagement(id);
  }

  @Override
  public PaginatedListTo<EngagementEto> findEngagementEtos(EngagementSearchCriteriaTo criteria) {

    return this.ucFindEngagement.findEngagementEtos(criteria);
  }

  @Override
  public EngagementEto saveEngagement(EngagementEto engagement) {

    return this.ucManageEngagement.saveEngagement(engagement);
  }

  @Override
  public boolean deleteEngagement(Long id) {

    return this.ucManageEngagement.deleteEngagement(id);
  }
}
