package holidays.engagementmanagement.logic.api.usecase;

import holidays.engagementmanagement.logic.api.to.EngagementEto;

/**
 * Interface of UcManageEngagement to centralize documentation and signatures of methods.
 */
public interface UcManageEngagement {

  /**
   * Deletes a engagement from the database by its id 'engagementId'.
   *
   * @param engagementId Id of the engagement to delete
   * @return boolean <code>true</code> if the engagement can be deleted, <code>false</code> otherwise
   */
  boolean deleteEngagement(Long engagementId);

  /**
   * Saves a engagement and store it in the database.
   *
   * @param engagement the {@link EngagementEto} to create.
   * @return the new {@link EngagementEto} that has been saved with ID and version.
   */
  EngagementEto saveEngagement(EngagementEto engagement);

}
