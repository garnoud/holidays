package holidays.engagementmanagement.logic.api.usecase;

import java.util.List;

import holidays.engagementmanagement.logic.api.to.EngagementEto;
import holidays.engagementmanagement.logic.api.to.EngagementSearchCriteriaTo;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

public interface UcFindEngagement {

  /**
   * Returns a Engagement by its id 'id'.
   *
   * @param id The id 'id' of the Engagement.
   * @return The {@link EngagementEto} with id 'id'
   */
  EngagementEto findEngagement(Long id);

  /**
   * Returns a paginated list of Engagements matching the search criteria.
   *
   * @param criteria the {@link EngagementSearchCriteriaTo}.
   * @return the {@link List} of matching {@link EngagementEto}s.
   */
  PaginatedListTo<EngagementEto> findEngagementEtos(EngagementSearchCriteriaTo criteria);

}
