package holidays.engagementmanagement.logic.api.to;

import holidays.engagementmanagement.common.api.Engagement;
import holidays.general.common.api.to.AbstractEto;

/**
 * Entity transport object of Engagement
 */
public class EngagementEto extends AbstractEto implements Engagement {

  private static final long serialVersionUID = 1L;

  private String name;

  @Override
  public String getName() {

    return name;
  }

  @Override
  public void setName(String name) {

    this.name = name;
  }

  @Override
  public int hashCode() {

    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {

    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    // class check will be done by super type EntityTo!
    if (!super.equals(obj)) {
      return false;
    }
    EngagementEto other = (EngagementEto) obj;
    if (this.name == null) {
      if (other.name != null) {
        return false;
      }
    } else if (!this.name.equals(other.name)) {
      return false;
    }
    return true;
  }
}
