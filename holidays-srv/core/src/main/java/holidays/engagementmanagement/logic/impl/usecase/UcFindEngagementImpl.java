package holidays.engagementmanagement.logic.impl.usecase;

import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import holidays.engagementmanagement.dataaccess.api.EngagementEntity;
import holidays.engagementmanagement.logic.api.to.EngagementEto;
import holidays.engagementmanagement.logic.api.to.EngagementSearchCriteriaTo;
import holidays.engagementmanagement.logic.api.usecase.UcFindEngagement;
import holidays.engagementmanagement.logic.base.usecase.AbstractEngagementUc;
import holidays.general.logic.api.UseCase;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * Use case implementation for searching, filtering and getting Engagements
 */
@Named
@UseCase
@Validated
@Transactional
public class UcFindEngagementImpl extends AbstractEngagementUc implements UcFindEngagement {

  /** Logger instance. */
  private static final Logger LOG = LoggerFactory.getLogger(UcFindEngagementImpl.class);

  @Override
  public EngagementEto findEngagement(Long id) {

    LOG.debug("Get Engagement with id {} from database.", id);
    return getBeanMapper().map(getEngagementDao().findOne(id), EngagementEto.class);
  }

  @Override
  public PaginatedListTo<EngagementEto> findEngagementEtos(EngagementSearchCriteriaTo criteria) {

    criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
    PaginatedListTo<EngagementEntity> engagements = getEngagementDao().findEngagements(criteria);
    return mapPaginatedEntityList(engagements, EngagementEto.class);
  }

}
