package holidays.engagementmanagement.service.impl.rest;

import javax.inject.Inject;
import javax.inject.Named;

import holidays.engagementmanagement.logic.api.Engagementmanagement;
import holidays.engagementmanagement.logic.api.to.EngagementEto;
import holidays.engagementmanagement.logic.api.to.EngagementSearchCriteriaTo;
import holidays.engagementmanagement.service.api.rest.EngagementmanagementRestService;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * The service implementation for REST calls in order to execute the logic of component {@link Engagementmanagement}.
 */
@Named("EngagementmanagementRestService")
public class EngagementmanagementRestServiceImpl implements EngagementmanagementRestService {

  @Inject
  private Engagementmanagement engagementmanagement;

  @Override
  public EngagementEto getEngagement(long id) {

    return this.engagementmanagement.findEngagement(id);
  }

  @Override
  public EngagementEto saveEngagement(EngagementEto engagement) {

    return this.engagementmanagement.saveEngagement(engagement);
  }

  @Override
  public void deleteEngagement(long id) {

    this.engagementmanagement.deleteEngagement(id);
  }

  @Override
  public PaginatedListTo<EngagementEto> findEngagementsByPost(EngagementSearchCriteriaTo searchCriteriaTo) {

    return this.engagementmanagement.findEngagementEtos(searchCriteriaTo);
  }

}