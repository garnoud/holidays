package holidays.engagementmanagement.service.api.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import holidays.engagementmanagement.logic.api.Engagementmanagement;
import holidays.engagementmanagement.logic.api.to.EngagementEto;
import holidays.engagementmanagement.logic.api.to.EngagementSearchCriteriaTo;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * The service interface for REST calls in order to execute the logic of component {@link Engagementmanagement}.
 */
@Path("/engagementmanagement/v1")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface EngagementmanagementRestService {

  /**
   * Delegates to {@link Engagementmanagement#findEngagement}.
   *
   * @param id the ID of the {@link EngagementEto}
   * @return the {@link EngagementEto}
   */
  @GET
  @Path("/engagement/{id}/")
  public EngagementEto getEngagement(@PathParam("id") long id);

  /**
   * Delegates to {@link Engagementmanagement#saveEngagement}.
   *
   * @param engagement the {@link EngagementEto} to be saved
   * @return the recently created {@link EngagementEto}
   */
  @POST
  @Path("/engagement/")
  public EngagementEto saveEngagement(EngagementEto engagement);

  /**
   * Delegates to {@link Engagementmanagement#deleteEngagement}.
   *
   * @param id ID of the {@link EngagementEto} to be deleted
   */
  @DELETE
  @Path("/engagement/{id}/")
  public void deleteEngagement(@PathParam("id") long id);

  /**
   * Delegates to {@link Engagementmanagement#findEngagementEtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding engagements.
   * @return the {@link PaginatedListTo list} of matching {@link EngagementEto}s.
   */
  @Path("/engagement/search")
  @POST
  public PaginatedListTo<EngagementEto> findEngagementsByPost(EngagementSearchCriteriaTo searchCriteriaTo);

}