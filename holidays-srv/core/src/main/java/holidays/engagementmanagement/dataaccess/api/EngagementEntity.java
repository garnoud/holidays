package holidays.engagementmanagement.dataaccess.api;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import holidays.engagementmanagement.common.api.Engagement;
import holidays.general.dataaccess.api.ApplicationPersistenceEntity;

/**
 * @author GARNOUD
 */
@Entity
@Table(name = "engagement")
public class EngagementEntity extends ApplicationPersistenceEntity implements Engagement {

  private String name;

  private static final long serialVersionUID = 1L;

  /**
   * @return name
   */
  @Column(name = "name")
  public String getName() {

    return this.name;
  }

  /**
   * @param name new value of {@link #getname}.
   */
  public void setName(String name) {

    this.name = name;
  }

}
