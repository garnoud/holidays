package holidays.engagementmanagement.dataaccess.api.dao;

import holidays.engagementmanagement.dataaccess.api.EngagementEntity;
import holidays.engagementmanagement.logic.api.to.EngagementSearchCriteriaTo;
import holidays.general.dataaccess.api.dao.ApplicationDao;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * Data access interface for Engagement entities
 */
public interface EngagementDao extends ApplicationDao<EngagementEntity> {

  /**
   * Finds the {@link EngagementEntity engagements} matching the given {@link EngagementSearchCriteriaTo}.
   *
   * @param criteria is the {@link EngagementSearchCriteriaTo}.
   * @return the {@link PaginatedListTo} with the matching {@link EngagementEntity} objects.
   */
  PaginatedListTo<EngagementEntity> findEngagements(EngagementSearchCriteriaTo criteria);
}
