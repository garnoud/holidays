package holidays.engagementmanagement.dataaccess.impl.dao;

import javax.inject.Named;

import com.mysema.query.alias.Alias;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.EntityPathBase;

import holidays.engagementmanagement.dataaccess.api.EngagementEntity;
import holidays.engagementmanagement.dataaccess.api.dao.EngagementDao;
import holidays.engagementmanagement.logic.api.to.EngagementSearchCriteriaTo;
import holidays.general.dataaccess.base.dao.ApplicationDaoImpl;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * This is the implementation of {@link EngagementDao}.
 */
@Named
public class EngagementDaoImpl extends ApplicationDaoImpl<EngagementEntity> implements EngagementDao {

  /**
   * The constructor.
   */
  public EngagementDaoImpl() {

    super();
  }

  @Override
  public Class<EngagementEntity> getEntityClass() {

    return EngagementEntity.class;
  }

  @Override
  public PaginatedListTo<EngagementEntity> findEngagements(EngagementSearchCriteriaTo criteria) {

    EngagementEntity engagement = Alias.alias(EngagementEntity.class);
    EntityPathBase<EngagementEntity> alias = Alias.$(engagement);
    JPAQuery query = new JPAQuery(getEntityManager()).from(alias);

    String name = criteria.getName();
    if (name != null) {
      query.where(Alias.$(engagement.getName()).eq(name));
    }
    return findPaginated(criteria, query, alias);
  }

}