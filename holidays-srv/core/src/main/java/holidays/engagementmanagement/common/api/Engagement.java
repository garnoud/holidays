package holidays.engagementmanagement.common.api;

import holidays.general.common.api.ApplicationEntity;

public interface Engagement extends ApplicationEntity {

  public String getName();

  public void setName(String name);

}
