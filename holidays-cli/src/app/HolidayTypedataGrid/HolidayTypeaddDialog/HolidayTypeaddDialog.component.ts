import { Component, Inject } from '@angular/core';
import { MdDialogRef, MD_DIALOG_DATA } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-HolidayType-add-dialog',
  templateUrl: './HolidayTypeaddDialog.component.html'
})

export class HolidayTypeAddDialogComponent {
  items: any;
  title: string;

  constructor(public dialogRef: MdDialogRef<HolidayTypeAddDialogComponent>,
              private translate: TranslateService,
              @Inject(MD_DIALOG_DATA) dialogData: any) {
                if (!dialogData) {
                  this.title = this.getTranslation('HolidayTypedatagrid.addTitle');
                  this.items = {
                    name: '',
                    writeable: ''
                  };
                } else {
                  this.title = this.getTranslation('HolidayTypedatagrid.editTitle');
                  this.items = dialogData;
                }
  }

  getTranslation(text: string): string {
    let value: string;
    this.translate.get(text).subscribe( (res) => {
        value = res;
    });
    return value;
  }
}
