import { TranslateService } from '@ngx-translate/core';
import { ITdDataTableColumn } from '@covalent/core';
import { Injectable } from '@angular/core'
import { HttpClient } from '../../shared/security/httpClient.service';
import { BusinessOperations } from '../../BusinessOperations';

@Injectable()
export class HolidayTypeDataGridService {

    constructor(private BO: BusinessOperations,
                private http: HttpClient) {
    }

    getData(size: number, page: number, searchTerms, sort: any[]) {
      let pageData = {
        pagination: {
          size: size,
          page: page,
          total: 1
        },
        name: searchTerms.name,
        writeable: searchTerms.writeable,
        sort: sort
      }
      return this.http.post(this.BO.postHolidayTypeSearch(), pageData)
                      .map(res => res.json());
    }

    saveData(data) {
      let obj = {
        id: data.id,
        name: data.name,
        writeable: data.writeable
      };

      return this.http.post(this.BO.postHolidayType(),  obj ).map(res => res.json());
    }

    deleteData(id) {
      return this.http.delete(this.BO.deleteHolidayType() + id)
    }

    searchData(criteria) {
      return this.http.post(this.BO.postHolidayTypeSearch(), { criteria: criteria }).map(res => res.json());
    }

}
