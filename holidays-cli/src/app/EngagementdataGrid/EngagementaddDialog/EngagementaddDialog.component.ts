import { Component, Inject } from '@angular/core';
import { MdDialogRef, MD_DIALOG_DATA } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-Engagement-add-dialog',
  templateUrl: './EngagementaddDialog.component.html'
})

export class EngagementAddDialogComponent {
  items: any;
  title: string;

  constructor(public dialogRef: MdDialogRef<EngagementAddDialogComponent>,
              private translate: TranslateService,
              @Inject(MD_DIALOG_DATA) dialogData: any) {
                if (!dialogData) {
                  this.title = this.getTranslation('Engagementdatagrid.addTitle');
                  this.items = {
                    name: ''
                  };
                } else {
                  this.title = this.getTranslation('Engagementdatagrid.editTitle');
                  this.items = dialogData;
                }
  }

  getTranslation(text: string): string {
    let value: string;
    this.translate.get(text).subscribe( (res) => {
        value = res;
    });
    return value;
  }
}
