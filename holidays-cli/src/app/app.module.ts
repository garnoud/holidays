import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { InitialPageComponent } from './initial-page/initial-page.component';
import { routing } from './app.routing';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CovalentModule } from './shared/covalent.module';
import { HttpModule, Http } from '@angular/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MaterialModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import 'hammerjs';
import { BusinessOperations } from './BusinessOperations';
import { HttpClient } from './shared/security/httpClient.service';
import { LoginService } from './login/shared/login.service';
import { AuthService } from './shared/security/auth.service';
import { HeaderService } from './header/shared/header.service';
import { AuthGuard } from './shared/security/auth-guard.service';
import { HolidayTypeDataGridComponent } from './HolidayTypedataGrid/HolidayTypedataGrid.component';
import { HolidayTypeAddDialogComponent } from './HolidayTypedataGrid/HolidayTypeaddDialog/HolidayTypeaddDialog.component';
import { HolidayTypeDataGridService } from './HolidayTypedataGrid/shared/HolidayTypedataGrid.service';
import { BankHolidayDataGridComponent } from './BankHolidaydataGrid/BankHolidaydataGrid.component';
import { BankHolidayAddDialogComponent } from './BankHolidaydataGrid/BankHolidayaddDialog/BankHolidayaddDialog.component';
import { BankHolidayDataGridService } from './BankHolidaydataGrid/shared/BankHolidaydataGrid.service';
import { EngagementDataGridComponent } from './EngagementdataGrid/EngagementdataGrid.component';
import { EngagementAddDialogComponent } from './EngagementdataGrid/EngagementaddDialog/EngagementaddDialog.component';
import { EngagementDataGridService } from './EngagementdataGrid/shared/EngagementdataGrid.service';


export function translateFactory(http: Http) {
    return new TranslateHttpLoader(http);
}
@NgModule({
    imports: [BrowserModule, BrowserAnimationsModule, CovalentModule, FormsModule, HttpModule, routing, MaterialModule, TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: translateFactory,
            deps: [Http]
        }
    })],
    declarations: [AppComponent, LoginComponent, HomeComponent, HeaderComponent, InitialPageComponent, HolidayTypeDataGridComponent, HolidayTypeAddDialogComponent, BankHolidayDataGridComponent, BankHolidayAddDialogComponent, EngagementDataGridComponent, EngagementAddDialogComponent],
    bootstrap: [AppComponent],
    providers: [HttpClient, AuthGuard, LoginService, HeaderService, AuthService, BusinessOperations, HolidayTypeDataGridService, BankHolidayDataGridService, EngagementDataGridService],
    entryComponents: [HolidayTypeAddDialogComponent, BankHolidayAddDialogComponent, EngagementAddDialogComponent]
}) export class AppModule {}