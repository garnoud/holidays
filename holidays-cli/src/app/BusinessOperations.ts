import { Observable } from 'rxjs/Rx';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { url } from '../assets/serverPath';


@Injectable() export class BusinessOperations {
    public serverPath = url;
    public servicesPath = this.serverPath;
    constructor(private http: Http) {}
    login() {
        return this.serverPath + 'login';
    }
    logout() {
        return this.serverPath + 'logout';
    }
    getCsrf() {
        return this.serverPath + 'security/v1/csrftoken';
    }
    postHolidayType() {
        return this.servicesPath + 'holidaymanagement/v1/holidaytype/';
    }
    postHolidayTypeSearch() {
        return this.servicesPath + 'holidaymanagement/v1/holidaytype/search';
    }
    deleteHolidayType() {
        return this.servicesPath + 'holidaymanagement/v1/holidaytype/';
    }
    postBankHoliday() {
        return this.servicesPath + 'holidaymanagement/v1/bankholiday/';
    }
    postBankHolidaySearch() {
        return this.servicesPath + 'holidaymanagement/v1/bankholiday/search';
    }
    deleteBankHoliday() {
        return this.servicesPath + 'holidaymanagement/v1/bankholiday/';
    }
    postEngagement() {
        return this.servicesPath + 'engagementmanagement/v1/engagement/';
    }
    postEngagementSearch() {
        return this.servicesPath + 'engagementmanagement/v1/engagement/search';
    }
    deleteEngagement() {
        return this.servicesPath + 'engagementmanagement/v1/engagement/';
    }
}