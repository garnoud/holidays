import { Component, Inject } from '@angular/core';
import { MdDialogRef, MD_DIALOG_DATA } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';


@Component({
    selector: 'app-BankHoliday-add-dialog',
    templateUrl: './BankHolidayaddDialog.component.html'
}) export class BankHolidayAddDialogComponent {
    items: any;
    title: string;
    constructor(public dialogRef: MdDialogRef < BankHolidayAddDialogComponent > , private translate: TranslateService, @Inject(MD_DIALOG_DATA) dialogData: any) {
        if (!dialogData) {
            this.title = this.getTranslation('BankHolidaydatagrid.addTitle');
            this.items = {
                beginDate: '',
                endDate: '',
                holidayTypeId: ''
            };
        } else {
            this.title = this.getTranslation('BankHolidaydatagrid.editTitle');
            this.items = dialogData;
        }
    }
    getTranslation(text: string): string {
        let value: string;
        this.translate.get(text).subscribe((res) => {
            value = res;
        });
        return value;
    }
}