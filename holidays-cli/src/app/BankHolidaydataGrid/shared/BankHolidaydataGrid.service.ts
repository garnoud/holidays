import { TranslateService } from '@ngx-translate/core';
import { ITdDataTableColumn } from '@covalent/core';
import { Injectable } from '@angular/core';
import { HttpClient } from '../../shared/security/httpClient.service';
import { BusinessOperations } from '../../BusinessOperations';


@Injectable() export class BankHolidayDataGridService {
    constructor(private BO: BusinessOperations, private http: HttpClient) {}
    getData(size: number, page: number, searchTerms, sort: any[]) {
        let pageData = {
            pagination: {
                size: size,
                page: page,
                total: 1
            },
            beginDate: searchTerms.beginDate,
            endDate: searchTerms.endDate,
            holidayTypeId: searchTerms.holidayTypeId,
            sort: sort
        };
        return this.http.post(this.BO.postBankHolidaySearch(), pageData).map(res => res.json());
    }
    saveData(data) {
        let obj = {
            id: data.id,
            beginDate: data.beginDate,
            endDate: data.endDate,
            holidayTypeId: data.holidayTypeId
        };
        return this.http.post(this.BO.postBankHoliday(), obj).map(res => res.json());
    }
    deleteData(id) {
        return this.http.delete(this.BO.deleteBankHoliday() + id)
    }
    searchData(criteria) {
        return this.http.post(this.BO.postBankHolidaySearch(), {
            criteria: criteria
        }).map(res => res.json());
    }
}