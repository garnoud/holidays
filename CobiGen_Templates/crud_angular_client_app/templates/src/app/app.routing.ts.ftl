import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './shared/security/auth-guard.service';
import { InitialPageComponent } from './initial-page/initial-page.component';
import { HomeComponent } from './home/home.component';
import { ${variables.etoName?cap_first}DataGridComponent } from './${variables.etoName}dataGrid/${variables.etoName}dataGrid.component';
